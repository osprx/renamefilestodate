# RenameFilesToDate

this Linux shell script renames all files with defined extensions in the entire directory, renamed to their creation date (timestamp of file) according to the following syntax: yymmddHHMMSS.ext
This shell script is useful to manage photos and videos in the photo album or media collection over a long period.

If there are multiple files with the same time stamp, so a sequential number is added to the file name, separated by an underscore.
For example, the files PIC456.JPG and PIC457.JPG have the same creation date and time of 2015-05-29 17:13:05,
this files will be renamed to 150529171305.JPG and 150529171305_1.JPG	

instructions:
copy the shell script to the ~/bin directory as root.

limitations:
-the shell script renames files in the entire directory only
-the shell script does not work with images transferred from android device (this files have not the real timestamp of their creation date)

known issues:
-files without extension are not correctly processed
	