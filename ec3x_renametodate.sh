#!/bin/bash

#---------------------------------------------------------------
#   Project name			ec3x_renametodate
#	Version					1.0 
#	Main File				ec3x_renametodate.sh
#	Project start date		2015-05-26	 
#	Created By				jwn
#	
#	Description:
#	this Linux shell script renames all files with defined extensions in the entire directory,
# 	renamed to their creation date (timestamp of file) according to the following syntax: yymmddHHMMSS.ext
# 	This shell script is useful to manage photos and videos in the photo album or media collection over a long period.
#
#	If there are multiple files with the same time stamp, so a sequential number is added to the file name, separated by an underscore.
#	For example, the files PIC456.JPG and PIC457.JPG have the same creation date and time of 2015-05-29 17:13:05,
#	this files will be renamed to 150529171305.JPG and 150529171305_1.JPG	
#
#	instructions:
#	copy the shell script to the ~/bin directory as root.
#
#	limitations:
#	-the shell script renames files in the entire directory only
#	-the shell script does not work with images transferred from android device (this files have not the real timestamp of their creation date)
#
#	known issues:
#	-files without extension are not correctly processed
#	
#
#---------------------------------------------------------------


#---------------------------------------------------------------
#   create files for testing
#	uncomment lines if script testing below required
#---------------------------------------------------------------
# delete all test files in testing directory
#echo "deleting old test files..."
#rm *.tsf; rm *.tsx
# create files for testing
#echo "generating new test files..."
#extension="tsf"
#for i in {1..9} 
# do  
    # normally, create files with the same timestamp
    # echo TestFile$i > file$i.$extension 
	#if (("$i" > 4 ))
	#then
		# create test files here with different timestamp, it works only with sleep x		
		#sleep 1
		#create file with different extension for testing purpose		
		#extension="tsx"
	#fi
#echo "Test File "file$i.$extension" created"	
#done


#---------------------------------------------------------------
#   jwn_renametodate.sh main script
#	rename files to their creation date
#---------------------------------------------------------------

# For Loop File Names With Spaces:
# http://www.cyberciti.biz/tips/handling-filenames-with-spaces-in-bash.html
# without this are filenames with spaces not correctly processed.
SAVEIFS=$IFS
IFS=$(echo -en "\n\b")

# user confirmation
# http://stackoverflow.com/questions/226703/how-do-i-prompt-for-input-in-a-linux-shell-script
current_dir=${PWD##*/} ; # current dir without full path
current_dir=${PWD} ; # current dir with full path
#while true; do
#read -p "Rename all files in "$current_dir" to their creation date? [Y/n]: " answer
#    case $answer in
#        [Y]* ) break;;
#        [Nn]* ) echo "mass renaming aborted by user." ; exit;;
#        * ) echo "Please answer Yes or no.";;
#    esac
#done #while true


# old code
# extensions=( tsf tsx jpg jpeg png bmp ico mp4 mpeg4 avi divx mov )
# element_count=${#extensions[*]}
# echo extensions element_count=$element_count
# echo extensions=${#extensions[*]
# for extension in $extensions
# do
# ls *.$extension
# for file in *.$extension ; do
# for file in *.* ; do

# for security reasons, define accepted file extensions here.
# .tsf and .tsx extensions are for testing purposes only
i=0
echo "check for file types..."

# note: 2>/dev/null redirects error messages to /dev/null
files=$( find *.{jpg,jpeg,png,bmp,ico,mp4,mpeg4,avi,divx,mov,JPG,JPEG,PNG,BMP,ICO,MP4,MPEG4,AVI,DIVX,MOV,tsf,tsx} 2>/dev/null ) 
#echo $files

for file in $files; do
#echo "for... file name: "$file
	# copy 'file' named to it's timestamp
	
	# build extension of $file variable
	# http://stackoverflow.com/questions/29707154/bash-rename-keep-extension-on-file-name-variable
	EXTENSION="${file##*.}"
	TIMESTAMP_FILENAME=$(date -r $file +"%Y%m%d%H%M%S").$EXTENSION
		
	# check if file exists	
	if [ -f "$TIMESTAMP_FILENAME" ]
	then
		counter=1		
		
		while [ -f "$TIMESTAMP_FILENAME" ]		
		do	# extend file name continuously with '_X' if file already exists
			#echo "source: $file is equal target: $TIMESTAMP_FILENAME"			
			TIMESTAMP_FILENAME=$(date -r $file +"%Y%m%d%H%M%S"_$counter).$EXTENSION
			#echo "new target: $TIMESTAMP_FILENAME"
			((counter++))
		done	#while [ -f "$TIMESTAMP_FILENAME" ]		
		
	fi		
		echo move $file to $TIMESTAMP_FILENAME		
		mv $file $TIMESTAMP_FILENAME

	#sleep 1
((i++))
done	# for file in *

IFS=$SAVEIFS

echo "$i files are processed."
#done   # for extension in $extensions

